Salut à toi!!!

Te voici sur mon site réalisé dans le cadre du cours [How To Make (almost) Any Experiment Using Digital Fabrication](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/class-website/)

Tu pourras voir l'avancée de mon travail sur cette page

## À propos de moi

![](images/esteban.png)

Moi c'est Esteban Welschen, je suis actuellement étudiant en Master ingénieur civil architecte à l'[École Polytechnique de Bruxelles](https://polytech.ulb.be/). Je suis le cours donné au Fablab de l'ULB dans le cadre de ma formation.

## Mon parcours

J’aime travailler avec mes mains depuis mon plus jeune âge. Petit, j’adorais déjà bricoler avec mes parents (j’ai notamment restauré du mobilier avec mon papa). Concevoir, construire, je pense que c’est ce qui m’a poussé à faire ces études. Vous pouvez voir un aperçu de ce en quoi consiste les études d’ingénieur civil architecte à la fin de cette section.

En dehors de mes études et mon côté bricoleur, je suis également musicien, j’ai suivi des cours de saxophone pendant 9 années et des cours d’orgue pendant deux ans.

Je suis également ce qu’on pourrait nommer un bon fêtard. Depuis mon arrivé à l’université, je n’ai cessé de m’investir dans les ASBL étudiantes qui organisent de nombreuses soirées.

### Vidéo Etudier Ingénieur civil Architecte

<iframe width="560" height="315" src="https://www.youtube.com/embed/zYqKn3LbV5I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Mon parcours et compétences

Mes études m’ont permis d’utiliser de nombreux logiciels de conception et numérisation de 2D et 3D (Sketchup, Autocad, Rhinocéros, Grasshopper, Kangaroo… ) mais aussi des outils de traitement d’images (Photoshop, InDesign, Gimp, Inkscape… ) ainsi qu'un peu de programmation aussi sur Python 3.0.
J’ai déjà eu l’occasion d’utiliser un lasercut pour la conception de maquettes. Vous pouvez voir ici un petit [portfolio](https://issuu.com/sdstudioulbvub/docs/suds_ma-1_bruface_masterplans_-_2019_ok/36) des projets réalisés dans le cadre du cours de studio de Master 1 (mon projet de groupe se nomme OXYGEN<sup>2</sup>)
