# 2. Conception Assistée par Ordinateur

Ce module sera consacré au design et à la modélisation d'un kit de construction paramétrique [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks).

## 2.1 Logiciels de conception 3D

J'ai à ce jour déjà utilisé certains logiciels de conception 3D. Il existe plusieurs sortes de logiciel. Dans ceux que j'ai déjà précédemment utilisés.

Il y a pour la modélisation sur base de conception de pixels :

- Blender

Il y a pour la modélisation sur base de conception vectoriel :

- Sketchup
- AutoCAD
- Rhinocéros

En vectoriel paramétrique il y a également le plugin pour Rhinocéros :

- Grasshopper

Pour ce cours je vais utiliser deux nouveaux logiciels qui sont [OpenSCAD](https://openscad.org/) et [FreeCAD](https://www.freecadweb.org/). Ces deux logiciels ont l'avantage d'être en **open source**.

## 2.2 OpenSCAD

Je voulais premièrement utiliser **OpenSCAD** car c'est une conception paramétrique. Il est donc beaucoup plus simple d' ajuster son modèle. C'est également le seul logiciel de conception 3D que je connaisse qui utilise du code sous forme de texte pour modéliser.

Après avoir fait quelques tests rapides sur le logiciel ainsi qu'en utilisant une [Cheat sheet](https://openscad.org/cheatsheet/), il m'est déjà possible de modéliser des choses simples comme un flexlink.

Je commence par définir mes variables en premier.

```
{
$fn=60;
radius_in=2.4; //intern radius lego
length_rec=8; // length rectangle lego
radius_ext=3.9; // radius of external lego
height=6; // height lego
betweenplot=3.2; // distance between two holes
width_rec=2*radius_ext;
length_flex=30;
width_flex=1;
marge=0.5;
}
```

Ensuite je créé le première partie de mon module (un rectangle + un demi cylindre). Je mets pour exemple la première partie de mon code. Je mettrai par la suite le code en entier, mais j'inclurai les photos intermédiaires du module étape par étape.

```
module hemi_rectangle ()
{cube([width_rec, length_rec/2, height]);
}
module cylindre ()
{
cylinder(height, radius_ext, radius_ext);
}
module cylrec ()
{union(){translate([-radius_ext, 0, 0]){hemi_rectangle();}cylindre();}}
cylrec ();
```
![ ](../images/module2/flex1.jpg)

Je crée ensuite un second cylindre et fait la différence entre mon premier module et ce nouveau cylindre.

![ ](../images/module2/flex2.jpg)

Je fais ensuite un miroir pour obtenir le clips pour lego.

![ ](../images/module2/flex3.jpg)

Je crée ma partie flexible qui se compose d'un simple rectangle. Et par une copie du premier clips et plusieurs transformations (translation, rotation) j'obtiens un premier flexlink.

![ ](../images/module2/flex4.jpg)
![ ](../images/module2/flex5.jpg)

Par une deuxième série de transformation j'obtiens ensuite mon flexlink final plus complexe.

![ ](../images/module2/flex6.jpg)

### 2.2.1 Code OpenSCAD

```
//* FILE: Angle module
// *
// * AUTHOR: Esteban Welschen <esteban.welschen@ulb.be>
// *
// * DATE: 2022-03-02
// *
//* LICENSE : Creative Commons Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
// * -----------

$fn=60;
radius_in=2.4; //intern radius lego (rayon du trou)
length_rec=8; // length rectangle lego (longueur entre les extrémités du raccord lego)
radius_ext=3.9; // radius of external lego (rayon extérieur, l'arrondie)
height=6; // height lego (hauteur de la pièce)
betweenplot=3.2; // distance between two holes
width_rec=2*radius_ext;
length_flex=30; //(longueur de la partie flexible)
width_flex=1; //(largeur de la partie flexible)
marge=0.5; // (paramètre d'erreur)
module hemi_rectangle () //Création d'un cube
{cube([width_rec, length_rec/2, height]);
}
module cylindre () //Création d'un cylindre
{
cylinder(height, radius_ext, radius_ext);
}
module cylrec () //demi cylindre + cube
{union(){translate([-radius_ext, 0, 0]){hemi_rectangle();}cylindre();}}
module hole () //Cylindre aux dimensions du trou
{cylinder (height+5, radius_in, radius_in);
}
module hemi_cylrec_h () // demi pièce de fication
{translate ([0,-length_rec/2,0]) {
    difference () {cylrec();translate ([0,0,-2.5]) {hole();}}}
}
module cylrec_h ()//module à 2 trous
{union(){mirror([0,1,0]){hemi_cylrec_h();}hemi_cylrec_h();}
}
module flex_part () //Partie pliable
{cube([width_flex,length_flex+2*marge,height]);}

module longi() //module longitudinal
{union(){cylrec_h();translate ([0,length_rec/2+radius_ext-marge,0]){flex_part();}translate ([0,length_rec+2*radius_ext-marge+length_flex,0]){cylrec_h();};}}

module perp() //module perpendiculaire
{union(){cylrec_h();translate ([-width_rec/2+marge,-width_flex/2,0])rotate(90){flex_part();}translate ([-width_rec+marge-length_flex,0,0]){cylrec_h();};}}

module perp_long () //combinason
{union(){longi();perp();}}
perp_long ();
```
## 2.3 freeCAD

Je n'ai pas encore utilisé freeCAD. Mais ayant utilisé d'autres logiciels de modélisations 3D utilisant les mêmes principes, je pense qu'il me sera plus facile de comprendre ce logiciel qu' OpenSCAD.


</p>
</div>
