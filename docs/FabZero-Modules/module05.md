# 5. Electronique 1 - Prototypage

Pour ce module il a été décidé d'utiliser le système de prototypage **Arduino** via un microcontrôleur.



## 5.1 Microcontrôleur Arduino

Le microcontrôleur utilisé est le suivant

![](../images/module5/arduino.jpg)

Le microcontrôleur est composé de différents types de broches : de broches analogiques pouvant lire des variations de tension entre 0V et 5V ( (`analogRead()`) et des broches numériques pouvant lire uniquement si le courant passe ou pas soit 1 ou 0 (`digitalRead()`), et pouvant également transmettre des informations binaires (`digitalWrite()`) ou transmettre des informations digitales en modulant la largeur des impulsions entre 1 et 0 (`analogWrite`)

Il existe également des broches de sources d'énergies et de terres.

## 5.2 Test basique

Le premier test effectué consiste à faire clignoter une petite led. Pour s'aider il suffit de suivre les instructions fournies avec le logiciel Arduino. Un petit [tutoriel](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink) donnant le code ainsi que le câblage est disponible .

![](../images/module5/circuit1.jpg)

Le résultat est réussi comme on peut le constater dans la vidéo suivante.

![](docs/images/module5/blink.mp4)

<video controls>
 <source src="../../images/module5/blink.mp4" type="video/mp4">
</video>

## 5.3 Alarm antivol

Pour mon propre projet Arduino, j'ai décidé de réaliser une alarme. En cas de mouvement une alarme se déclencherait et une lumière s'allumerait. L'alarme s'éteint quand l'objet a retrouvé sa position initiale mais la lumière reste allumée tant que le bouton reset n'a pas été poussé.

Le code arduino :

```
/* FILE: Alarm sensor
 *
 * AUTHOR: Esteban Welschen <esteban.welschen@ulb.be>
 *
 * DATE: 2022-03-22
 *
 * LICENSE : Creative Commons Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
 * -----------
 *
 * Detects if the sensor has been tilted or not and
 * lights up the LED and the Buzz if so. If someone incline the tilt sensor the buzz rings,
 * the buzz stops if the tilt is returned to its original position.
 * The LED still lights up even if the tilt is returned to its position.
 * To switch off the light the reset button needs to be pushed.
 *
 */

int ledPin = 13;
int buzzPin = 8;
int inPin = 7;
int value = 0;

void setup()
{
  pinMode(ledPin, OUTPUT);              // initializes digital pin 13 as output
  pinMode(buzzPin, OUTPUT);             // initializes digital pin 8 as output
  pinMode(inPin, INPUT);                // initializes digital pin 7 as input
}
void loop()
{
  value = digitalRead(inPin);
  if (digitalRead(inPin) == 1) {
    digitalWrite(ledPin, value);
    digitalWrite(buzzPin, value);
  }
  else {
    digitalWrite(buzzPin, value);
  }
}
```
La vidéo du branchage ainsi que le fonctionnement se trouve juste en dessous.

Les composants utilisés sont :

| Element | fonction
| :--- | :---
| Un buzzer | reçoit un signal numérique binaire
| Une led | reçoit un signal numérique binaire passant par une résistance
| Un capteur d'inclinaison | Détecte l'inclinaison du module et envoie un signal numérique
| Un bouton poussoir | relié à un pin reset il permet de remettre le module à 0

![](docs/images/module5/alarm.mp4)

<video controls>
 <source src="../../images/module5/alarm.mp4" type="video/mp4">
</video>

## 5.4 Projet thermostat

Pour ce second projet Arduino, j'ai décidé de réaliser un thermostat. Ce thermostat est réglé sur une température de 22°C, une lumière verte s'allume si la température est atteinte. Une certaine marge est cependant acceptée, entre 21,5°C et 22,5°C la lumière reste verte. Si la température dépasse 22,5, la lumière devient rouge. Et si elle passe en dessous des 21.5 elle devient bleue.

Pour transformer le voltage reçu en température en degré celsius cela est relativement simple, il suffit de faire

- Temperature (°C) = V<sub>OUT</sub> / 10

On peut trouver ce rapport dans la [data sheet](https://www.makerguides.com/wp-content/uploads/2020/10/LM35-datasheet.pdf) du thermomètre LM35

Attention cependant que l'entrée analogique d'un Arduino convertit les tensions d'entrées (0V à 5V) en entier (0 à 1023) où une unité représentera donc 4,9 mV. Comme l'explique cette [page](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/) du site Arduino à propos de la fonction ```analogRead()```

Il faut donc convertir depuis "10-bit analog to digital converter (**ADC**)"
notre V<sub>OUT</sub>.

La formule à utiliser est la suivante :

V<sub>OUT</sub>.  = reading from ADC * (V<sub>ref</sub>  / 1024)

V<sub>ref</sub> est égale à 5V sur un Arduino uno.


Le code Arduino :

```
//* FILE = Thermostat_RGB
//* AUTHOR = Estban Welschen <esteban.welschen@ulb.be>
//* DATE = 2022-03-29
//* LICENSE = Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) (https://creativecommons.org/licenses/by-nc-sa/4.0/)
//* This work, "Thermostat_RGB", is a derivative of
//* "LM35 analog temperature sensor with Arduino example code"
//* by Benne de Bakker, used under CC BY-NC-SA.
//* "Thermostat_RGB" is licensed under CC BY-NC-SA by [Esteban Welschen].

// Define to which pin of the Arduino the output of the LM35 is connected:
#define sensorPin A0

int red_light_pin= 11;
int green_light_pin = 10;
int blue_light_pin = 9;
int temperature_set = 22; //Temperature reference for the thermostat

void setup() {
  // Begin serial communication at a baud rate of 9600:
  Serial.begin(9600);
  pinMode(red_light_pin, OUTPUT);
  pinMode(green_light_pin, OUTPUT);
  pinMode(blue_light_pin, OUTPUT);

}

void loop() {
  // Get a reading from the temperature sensor:
  int reading = analogRead(sensorPin);

  // Convert the reading into voltage:
  float voltage = reading * (5000 / 1024.0);

  // Convert the voltage into the temperature in degree Celsius:
  float temperature = voltage / 10;
  // Light the led in blue color range if temperature under 21,5°C
  if (temperature < temperature_set-0.5) {
    digitalWrite (blue_light_pin, HIGH);
    digitalWrite (green_light_pin, LOW);
    digitalWrite (red_light_pin, LOW);
  }
  // Light the led in red color range if temperature above 22,5°C
  if (temperature > temperature_set+0.5) {
    digitalWrite (red_light_pin, HIGH);
    digitalWrite (green_light_pin, LOW);
    digitalWrite (blue_light_pin, LOW);
  }
  // Light the led in green color range if temperature between 21,5°C & 22,5°C
  if ((temperature <= temperature_set+0.5) && (temperature >=temperature_set-0.5)){
    digitalWrite (green_light_pin, HIGH);
    digitalWrite (blue_light_pin, LOW);
    digitalWrite (red_light_pin,LOW);
  }
  // Print the temperature in the Serial Monitor:
  Serial.print(temperature);
  Serial.print(" \xC2\xB0"); // shows degree symbol
  Serial.println("C");

  delay(1000); // wait a second between readings

}
```

Pour le branchage du module, il s'agit uniquement de la combinaison classique du branchage d'une led RGB et du branchage d'un thermomètre LM35

![](../images/module5/rgb.jpg)
![](../images/module5/temp.jpg)

La vidéo du fonctionnement se trouve juste en dessous.

Les composants utilisés sont :

| Element | fonction
| :--- | :---
| Une led RGB | Peut produire de la lumière de différente couleur à partir d'une base de rouge, vert et bleu, chaque couleur reçoit un signal binaire
| Un thermomètre LM35 | Prend la température et la convertit en Voltage
| Trois résistances 220Ω | Diminue l'intensité du courant pour pas faire brûler la led

![](docs/images/module5/thermostat.mp4)

<video controls>
 <source src="../../images/module5/thermostat.mp4" type="video/mp4">
</video>

</p>
</div>
