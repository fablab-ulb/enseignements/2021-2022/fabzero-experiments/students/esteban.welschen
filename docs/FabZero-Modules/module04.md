# 4. Impression 3D

Pour ce module nous allons tenter d'imprimer le [FlexLink](https://www.compliantmechanisms.byu.edu/flexlinks) qui a été modélisé dans le module 2.

## 4.1 Impression 3D

Pour l'impression 3D, j'ai utilisé l'imprimante 3D d'un ami (une imprimante creality Ender 3).

![ ](../images/module4/print1.jpg)

J'ai utilisé le logiciel [MatterControl](https://www.matterhackers.com/store/l/mattercontrol/sk/MKZGTDW6). Les paramètres sont assez similaires à ceux du logiciel PrusaSlicer que l’on a vu au cours. On peut voir son fonctionnement dans la vidéo suivante :

<iframe width="560" height="315" src="https://www.youtube.com/embed/UfvyV03WJG8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Avant d'imprimer ma pièce, j'ai vérifié une dernière fois les paramètres de mon modèle. J'ai consulté les GitLab des années précédentes pour voir si la dimension du trou devait être exactement celle du lego ou si on devait prendre une certaine marge. Le rayon d'un trou de lego fait 2,4 mm. Cependant les étudiants précédents augmentaient le rayon de 0,1mm soit un rayon de 2,5 mm. J'ai également remarqué une petite erreur de mon code que j'ai corrigé.

Le nouveau code est celui-ci :

```
//* FILE: Angle module v.2
// *
// * AUTHOR: Esteban Welschen <esteban.welschen@ulb.be>
// *
// * DATE: 2022-03-02
// * Update: 2022-13-04

//* LICENSE : Creative Commons Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
// * -----------

$fn=60;
radius_in=2.5; //intern radius lego (rayon du trou)
length_rec=8; // length rectangle lego (longueur entre les extrémités du raccord lego)
radius_ext=3.9; // radius of external lego (rayon extérieur, l'arrondie)
height=6; // height lego (hauteur de la pièce)
betweenplot=3.2; // distance between two holes
width_rec=2*radius_ext;
length_flex=30; //(longueur de la partie flexible)
width_flex=1; //(largeur de la partie flexible)
marge=0.5; // (paramètre d'erreur)
module hemi_rectangle () //Création d'un cube
{cube([width_rec, length_rec/2, height]);
}
module cylindre () //Création d'un cylindre
{
cylinder(height, radius_ext, radius_ext);
}
module cylrec () //demi cylindre + cube
{union(){translate([-radius_ext, 0, 0]){hemi_rectangle();}cylindre();}}

module hole () //Cylindre aux dimensions du trou
{cylinder (height+5, radius_in, radius_in);
}
module hemi_cylrec_h () // demi pièce de fication
{translate ([0,-length_rec/2,0]) {
    difference () {cylrec();translate ([0,0,-2.5]) {hole();}}}
}
module cylrec_h ()//module à 2 trous
{union(){mirror([0,1,0]){hemi_cylrec_h();}hemi_cylrec_h();}
}
module flex_part () //Partie pliable
{cube([width_flex,length_flex+2*marge,height]);}

module longi() //module longitudinal
{union(){cylrec_h();translate ([-width_flex/2,length_rec/2+radius_ext-marge,0]){flex_part();}translate ([0,length_rec+2*radius_ext-marge+length_flex,0]){cylrec_h();};}}

module perp() //module perpendiculaire
{union(){cylrec_h();translate ([-width_rec/2+marge,-width_flex/2,0])rotate(90){flex_part();}translate ([-width_rec+marge-length_flex,0,0]){cylrec_h();};}}

module perp_long () //combinason
{union(){longi();perp();}}
perp_long ();

```

Puis, pour l'utiliser sur le logiciel MatterControl j'ai exporté mon fichier en stl [(stéréolithographie)](https://fr.wikipedia.org/wiki/Fichier_de_st%C3%A9r%C3%A9olithographie). J'ai ensuite paramétré mon module avec les paramètres suivant (voir photo), j'ai choisis de faire un remplissage plein pour avoir quelque chose de solide. D'autant plus que l'impression ne durait qu'environ 30 min.

![ ](../images/module4/impr1.jpg)
![ ](../images/module4/impr3.jpg)

Lors de la première tentative d'impression, j'avais oublié de retirer le paramètre de support, qui dans notre cas est inutile. On peut voir le support sur l'image suivante en rouge.

![ ](../images/module4/impr2.jpg)

Ensuite le fichier G-code a été mis sur une carte SD et l'impression a pu être lancé

![ ](../images/module4/print2.jpg)

![](docs/images/module4/print3.mp4)

<video controls>
 <source src="../../images/module4/print3.mp4" type="video/mp4">
</video>

J'ai obtenu un résultat assez propre et proche des caractéristiques que je souhaitais.

![ ](../images/module4/print4.jpg)

![](docs/images/module4/print5.mp4)

<video controls>
 <source src="../../images/module4/print5.mp4" type="video/mp4">
</video>

## 4.2 Mécanisme lego

La pièce construite au point précédent doit pouvoir à l'aide de quelques pièces Lego, servir de mécanisme.
Le mécanisme construit doit posséder plusieurs équilibres stables, c'est-à-dire des positions vers lesquelles le mécanisme va tendre.

Pour le mécanisme que j'ai construit on peut distinguer trois positions stables.

1. La première position ci-dessous représente le mécanisme ou la pièce est au repos

![ ](../images/module4/lego1.jpg)

2. La seconde position représente le mécanisme lorsque la pièce est mise en compression

![ ](../images/module4/lego2.jpg)

3. Enfin la troisième position représente le mécanisme lorsque la pièce est mis en tension

![ ](../images/module4/lego3.jpg)

On peut voir dans la vidéo suivante les différentes positions du mécanisme ainsi que ses phases transitoires. Malheureusement le mécanisme semble un peu mou et a parfois du mal à reprendre sa position de repos. Cela est dû à une certaine fatigue du plastique PLA (j'ai beaucoup testé mon module et aitprobablement excédé les limites du matériau ).

![](docs/images/module4/lego4.mp4)

<video controls>
 <source src="../../images/module4/lego4.mp4" type="video/mp4">
</video>





</p>
</div>
