//* FILE: Angle module
// * 
// * AUTHOR: Esteban Welschen <esteban.welschen@ulb.be>
// * 
// * DATE: 2022-03-02
// * 
//* LICENSE : Creative Commons Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
// * -----------
 
$fn=60;
radius_in=2.4; //intern radius lego (rayon du trou)
length_rec=8; // length rectangle lego (longueur entre les extrémités du raccord lego)
radius_ext=3.9; // radius of external lego (rayon extérieur, l'arrondie)
height=6; // height lego (hauteur de la pièce)
betweenplot=3.2; // distance between two holes
width_rec=2*radius_ext;
length_flex=30; //(longueur de la partie flexible)
width_flex=1; //(largeur de la partie flexible)
marge=0.5; // (paramètre d'erreur)
module hemi_rectangle () //Création d'un cube
{cube([width_rec, length_rec/2, height]);
}
module cylindre () //Création d'un cylindre
{
cylinder(height, radius_ext, radius_ext);
}
module cylrec () //demi cylindre + cube
{union(){translate([-radius_ext, 0, 0]){hemi_rectangle();}cylindre();}}
module hole () //Cylindre aux dimensions du trou
{cylinder (height+5, radius_in, radius_in);
}
module hemi_cylrec_h () // demi pièce de fication
{translate ([0,-length_rec/2,0]) {
    difference () {cylrec();translate ([0,0,-2.5]) {hole();}}}
}
module cylrec_h ()//module à 2 trous
{union(){mirror([0,1,0]){hemi_cylrec_h();}hemi_cylrec_h();}
}
module flex_part () //Partie pliable
{cube([width_flex,length_flex+2*marge,height]);}

module longi() //module longitudinal
{union(){cylrec_h();translate ([0,length_rec/2+radius_ext-marge,0]){flex_part();}translate ([0,length_rec+2*radius_ext-marge+length_flex,0]){cylrec_h();};}}

module perp() //module perpendiculaire
{union(){cylrec_h();translate ([-width_rec/2+marge,-width_flex/2,0])rotate(90){flex_part();}translate ([-width_rec+marge-length_flex,0,0]){cylrec_h();};}}

module perp_long () //combinason
{union(){longi();perp();}}
perp_long ();
