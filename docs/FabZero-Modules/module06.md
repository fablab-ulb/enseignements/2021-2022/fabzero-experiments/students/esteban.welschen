# 6. Electronique 2 - Fabrication

Pour ce module nous allons fabriquer notre propre reproduction d'un Arduino connectable par USB

## 6.1 Souder les éléments sur le board

Dans ce module nous avons appris à souder notre propre board afin de fabriquer notre propre carte de développement de microcontrôleur. Nous allons souder nos éléments à l'aide d'un fer à souder.

![](../images/module6/soud1.jpg)

Les éléments fournis sont :

- un board
- [un circuit intégré (IC)](https://www.microchip.com/en-us/product/ATSAMD11C14)
- un régulateur
- Deux capacités de 3,3V
- une led verte
- une led rouge
- Deux résistances pour les 2 leds
- une résistance de protection
- Deux broches à 8 pins

**Le board**

![](../images/module6/soud2.jpg)

**Soudure du circuit intégré**

![](../images/module6/soud3.jpg)

**Soudure du régulateur**

![](../images/module6/soud4.jpg)

**Soudure d'une capacité**

![](../images/module6/soud5.jpg)

**Soudure de la resistance de protection**

![](../images/module6/soud6.jpg)

**Soudure des résistances des leds**

Les résistances des leds sont placées avant les autres pièces car il serait plus compliqué de les souder par la suite par manque de place.

![](../images/module6/soud7.jpg)

**Soudure de tous les autres éléments**

![](../images/module6/soud8.jpg)
![](../images/module6/soud9.jpg)

## 6.2 Uploader le bootloader et vérifier le bon fonctionnement de la carte de développement de microcontrôleur

Le [bootloader](https://github.com/mattairtech/ArduinoCore-samd/blob/master/bootloaders/zero/binaries/sam_ba_Generic_D11C14A_SAMD11C14A.bin) a pu être uploader sans difficulté. Malgré cela, quand on le branche au port usb de notre carte de développement celui-ci semble se connecter puis se déconnecter immédiatement après. Toutes les soudures ont été vérifiées à l'aide d'un multimètre et tout a bien été soudé. Une possibilité serait que le circuit intégré ait subi des dommages internes suite à une surchauffe lors de la soudure de ce dernier.


</p>
</div>
