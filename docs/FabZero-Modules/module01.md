# 1. Documentation

Pour documenter le travail réalisé dans le cadre du cours "How To Make (almost) Any Experiments", j'utilise [GitLab](https://about.gitlab.com/?utm_medium=cpc&utm_source=google&utm_campaign=brand_emea_pr_rsa_br_exact&utm_content=homepage_digital_x-pr_english_&_bt=363211725518&_bk=gitlab&_bm=e&_bn=g&_bg=75294586319&gclid=Cj0KCQjw3v6SBhCsARIsACyrRAkRBVqsGJspViR2FlxYZYhJaoGHMAerITNloPgCLUEGvIPgM6_9BzEaAq00EALw_wcB) via [Git](https://git-scm.com/).

## 1.1 Git & GitLab

Pour éditer sur GitLab il est plus simple de travailler en local (sur son propre ordinateur) et d’envoyer par la suite les mises à jour aux serveurs.

La première étape était d'installer GIT via le terminal. La procédure y est expliquée [ici](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git).
Une fois **git** installé il est conseillé d'utiliser une clef **SSH** (SSH pour [Secure Shell](https://fr.wikipedia.org/wiki/Secure_Shell)) afin de faciliter les envois, il s'agit en fait d'un protocole de communication sécurisé entre un environnement privé et un serveur.  Pour créer votre clef SSH vous pouvez suivre ces [instructions](https://docs.gitlab.com/ee/user/ssh.html).

Lorsque Git est installé , il est maintenant possible de travailler en local et d'envoyer les mises à jour sur le site au fur et à mesure.

Pour cela, il suffit de lancer les commandes suivantes :

| Instruction | Description
| :--- | :---
| `git clone git@gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/esteban.welschen.git` | pour copier le contenu du GitLab
| `git add -A` | pour enregistrer le tout
| `git commit -m "comment"` | pour confirmer le tout avant d'envoyer sur le serveur
| `git push` | pour envoyer les modifications au serveurs
| `git pull`| pour recevoir en local les modifications apportées sur le serveur qui n'aurait pas été mis à jour en local

### 1.1.1 Difficultés rencontrés

Mes premiers git push n'ont pas fonctionné. Cela était dû à un chemin mal défini. En rectifiant le chemin du dossier source les modifications se sont envoyées avec succès.

Il est possible de vérifier le chemin et d'en définir un nouveau en effectuant dans le terminal :

| Commande | Fonction
| :--- | :---
| `ls`| Affiche le contenu du répertoire
| `cd`| Permet de revenir au répertoire /home/utilisateur
| `cd -` | Permet de revenir au répertoire précédent
| `cd /` | Permet de remonter à la racine de l'ensemble du système de fichiers
| `cd`suivi du nom du fichier | Permet d'accéder au fichier sous-jacent voulu

`ls` pour "list"

`cd`pour "change directory"

## 1.2 Markdown / Atom

Pour la modification et l'édition des fichiers Markdown (.md) via [Atom](https://atom.io/) qui utilise une syntaxe simplifiée. [Markdown](https://fr.wikipedia.org/wiki/Markdown#:~:text=Markdown%20est%20un%20langage%20de,format%C3%A9%20par%20des%20instructions%20particuli%C3%A8res.) est un type de langage informatique dit de balisage léger.
Pour m'aider à utiliser cette syntaxe qui m'était inconnue j'utilise une [Cheat Sheet](https://www.markdownguide.org/cheat-sheet/#basic-syntax).

| Element | 	Markdown Syntax
| :--- | :---
| Heading | `# H1 ## H2 ### H3`
| Bold| `**bold text**`
| Italic | `*italicized text*`
| Blockquote | `> blockquote`
| Ordered List | `1. First item 2. Second item 3. Third item`
| Unordered List | `- First item - Second item - Third item`
| Horizontal Rule | `---`
| Link | `[title](https://www.example.com)`
| Image | `![alt text](image.jpg)`
| Subscript | 	`H~2~O` ou `H<sup>2</sup>O`
| Superscript | 	`X^2^` ou `X<sup>2</sup>`



## 1.3 MkDocs

Malheureusement, je n'ai pas réussi à installer correctement [MkDocs](https://www.mkdocs.org/) pour une raison encore inconnue encore. Il semble que MkDocs soit installé mais le terminal ne le trouve pas.

```
{
  MacBook-Pro-de-Welschen:esteban.welschen welschenesteban$ pip3.9 install mkdocs
Collecting mkdocs
  Downloading mkdocs-1.2.3-py3-none-any.whl (6.4 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 6.4/6.4 MB 2.8 MB/s eta 0:00:00
Collecting PyYAML>=3.10
  Downloading PyYAML-6.0-cp39-cp39-macosx_10_9_x86_64.whl (197 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 197.6/197.6 KB 1.1 MB/s eta 0:00:00
Collecting pyyaml-env-tag>=0.1
  Downloading pyyaml_env_tag-0.1-py3-none-any.whl (3.9 kB)
Collecting click>=3.3
  Downloading click-8.0.4-py3-none-any.whl (97 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 97.5/97.5 KB 1.2 MB/s eta 0:00:00
Collecting Jinja2>=2.10.1
  Downloading Jinja2-3.0.3-py3-none-any.whl (133 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 133.6/133.6 KB 1.2 MB/s eta 0:00:00
Collecting ghp-import>=1.0
  Downloading ghp_import-2.0.2-py3-none-any.whl (11 kB)
Collecting Markdown>=3.2.1
  Downloading Markdown-3.3.6-py3-none-any.whl (97 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 97.8/97.8 KB 1.2 MB/s eta 0:00:00
Collecting packaging>=20.5
  Downloading packaging-21.3-py3-none-any.whl (40 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 40.8/40.8 KB 474.7 kB/s eta 0:00:00
Collecting importlib-metadata>=3.10
  Downloading importlib_metadata-4.11.3-py3-none-any.whl (18 kB)
Collecting watchdog>=2.0
  Downloading watchdog-2.1.6-cp39-cp39-macosx_10_9_x86_64.whl (85 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 85.6/85.6 KB 1.4 MB/s eta 0:00:00
Collecting mergedeep>=1.3.4
  Downloading mergedeep-1.3.4-py3-none-any.whl (6.4 kB)
Collecting python-dateutil>=2.8.1
  Downloading python_dateutil-2.8.2-py2.py3-none-any.whl (247 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 247.7/247.7 KB 2.6 MB/s eta 0:00:00
Collecting zipp>=0.5
  Downloading zipp-3.7.0-py3-none-any.whl (5.3 kB)
Collecting MarkupSafe>=2.0
  Downloading MarkupSafe-2.1.1-cp39-cp39-macosx_10_9_x86_64.whl (13 kB)
Collecting pyparsing!=3.0.5,>=2.0.2
  Downloading pyparsing-3.0.7-py3-none-any.whl (98 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 98.0/98.0 KB 1.4 MB/s eta 0:00:00
Collecting six>=1.5
  Downloading six-1.16.0-py2.py3-none-any.whl (11 kB)
Installing collected packages: zipp, watchdog, six, PyYAML, pyparsing, mergedeep, MarkupSafe, click, pyyaml-env-tag, python-dateutil, packaging, Jinja2, importlib-metadata, Markdown, ghp-import, mkdocs
  WARNING: The script watchmedo is installed in '/Library/Frameworks/Python.framework/Versions/3.9/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  WARNING: The script markdown_py is installed in '/Library/Frameworks/Python.framework/Versions/3.9/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  WARNING: The script ghp-import is installed in '/Library/Frameworks/Python.framework/Versions/3.9/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  WARNING: The script mkdocs is installed in '/Library/Frameworks/Python.framework/Versions/3.9/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
Successfully installed Jinja2-3.0.3 Markdown-3.3.6 MarkupSafe-2.1.1 PyYAML-6.0 click-8.0.4 ghp-import-2.0.2 importlib-metadata-4.11.3 mergedeep-1.3.4 mkdocs-1.2.3 packaging-21.3 pyparsing-3.0.7 python-dateutil-2.8.2 pyyaml-env-tag-0.1 six-1.16.0 watchdog-2.1.6 zipp-3.7.0
MacBook-Pro-de-Welschen:esteban.welschen welschenesteban$ mkdocs --version
-bash: mkdocs: command not found
}
```

## 1.4 Traitement d'images

Pour une question d’optimisation de l’espace, il est important de ne pas uploader des fichiers trop lourds. Il d’ailleurs possible de diminuer la taille des images et vidéos que l’on souhaite utiliser. Pour les images il est possible de diminuer leur taille en utilisant soit un logiciel de traitement d'image comme Photoshop ou Gimp (pour les images pixelisés) soit directement via le terminal avec [Graphics-Magick](http://www.graphicsmagick.org/).

### 1.4.1 Photoshop

J'ai opté pour photoshop comme logiciel de traitement d'images plutôt que [Gimp](https://www.gimp.org/) qui est pourtant open source pour une question d'habitude et d'interface que je préfère.

Dans Photoshop il est possible de changer la résolution d’image de manière très simple. Il faut simplement aller dans l’onglet (image/image size) où on peut régler la résolution d’une image et ainsi diminuer sa taille.

Pour ma photo de présentation, j’ai par exemple choisis une résolution d’image de 300px x 300px contre 2897px x 2897px. On passe ainsi d’une image de 4,6 Mo à une image de 103 Ko.


![](../images/module1/photoshop4.jpg)

Une autre possibilité encore plus rapide est d'aller directement dans (File/export/export as), où l'on peut directement choisir la résolution ainsi que les paramètres de compression d'image.

![](../images/module1/photoshop5.jpg)

En jpeg, il est également possible de diminuer la taille du fichier en gardant le même nombre de pixels. Lorsque l’on exporte le fichier, on va choisir une certaine qualité. Le fichier va alors se compresser. On perd néanmoins en qualité de lecture.

Par exemple les deux images jpg suivantes ont toutes deux une même résolution de 300px x 300px. En revanche, la première a une taille de 87ko octet et la deuxième a une taille de 6ko.

![](../images/module1/esteban1.jpg)
![](../images/module1/esteban2.jpg)

### 1.4.2 GraphicsMagick

J’ai voulu utiliser [Graphicsmagick](http://www.graphicsmagick.org/) afin d’optimiser mon temps, mais il semble que quelque chose ne fonctionne pas. Je dois donc continuer à utiliser Photoshop.

Voici un exemple provenant de mon terminal après avoir téléchargé et installé graphicsmagick.

```
{
MacBook-Pro-de-Welschen:module1 welschenesteban$ gm identify photoshop1.png
gm identify: No decode delegate for this image format (photoshop1.png).
gm identify: Request did not return an image.
MacBook-Pro-de-Welschen:module1 welschenesteban$ gm convert -size 120x120 photoshop1.png -resize 120x120
gm convert: No decode delegate for this image format (photoshop1.png).
MacBook-Pro-de-Welschen:module1 welschenesteban$ gm identify test.jpg
gm identify: No decode delegate for this image format (test.jpg).
gm identify: Request did not return an image.
}
```

Lorsque l'on vérifie la version le terminal nous renvoie ceci :

```
{
MacBook-Pro-de-Welschen:module1 welschenesteban$ gm version
GraphicsMagick 1.3.37 2021-12-12 Q8 http://www.GraphicsMagick.org/
Copyright (C) 2002-2020 GraphicsMagick Group.
Additional copyrights and licenses apply to this software.
See http://www.GraphicsMagick.org/www/Copyright.html for details.

Feature Support:
  Native Thread Safe         yes
  Large Files (> 32 bit)     yes
  Large Memory (> 32 bit)    yes
  BZIP                       yes
  DPS                        no
  FlashPix                   no
  FreeType                   yes
  Ghostscript (Library)      no
  JBIG                       no
  JPEG-2000                  no
  JPEG                       no
  Little CMS                 no
  Loadable Modules           no
  Solaris mtmalloc           no
  Google perftools tcmalloc  no
  OpenMP                     no
  PNG                        no
  TIFF                       no
  TRIO                       no
  Solaris umem               no
  WebP                       no
  WMF                        no
  X11                        no
  XML                        yes
  ZLIB                       yes

Host type: x86_64-apple-darwin20.5.0

Configured using the command:
  ./configure

Final Build Parameters:
  CC       = gcc
  CFLAGS   = -g -O2 -Wall -D_THREAD_SAFE
  CPPFLAGS = -I/opt/X11/include/freetype2 -I/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include
  CXX      = g++
  CXXFLAGS = -D_THREAD_SAFE
  LDFLAGS  = -L/opt/X11/lib -L/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/lib
  LIBS     = -lfreetype -lbz2 -lxml2 -lz -lm -lpthread
}
```
On peut cependant déjà identifier les différentes commandes en faisant `gm -help`
Ce à quoi on reçoit les commandes suivantes :
```
{      
batch - issue multiple commands in interactive or batch mode
benchmark - benchmark one of the other commands
compare - compare two images
composite - composite images together
conjure - execute a Magick Scripting Language (MSL) XML script
convert - convert an image or sequence of images
help - obtain usage message for named command
identify - describe an image or image sequence
mogrify - transform an image or sequence of images
montage - create a composite image (in a grid) from separate images
time - time one of the other commands
version - obtain release version
}
```
## 1.5 Traitement de vidéos

Pour les vidéos j'ai décidé d'utiliser [HandBrake](https://handbrake.fr/).
On peut régler la résolution des vidéos ainsi que leur taille comme on peut le voir sur la photo suivante.

![](../images/module1/video.jpg)

Pour mettre les vidéos en ligne il faut les implémenter de la manière suivante dans atom

Pour que la vidéo soit lisible dans l'environement GitLab
```
![](docs/images/module5/blink.mp4)
```

Pour que la vidéo soit lisible sur le site
```
<video controls>
 <source src="../../images/module5/blink.mp4" type="video/mp4">
</video>
```

</p>
</div>
