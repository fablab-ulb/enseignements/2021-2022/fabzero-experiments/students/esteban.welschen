//* This work, "Thermostat_RGB", is a derivative of 
//* "LM35 analog temperature sensor with Arduino example code" 
//* by Benne de Bakker, used under CC BY-NC-SA. 
//* "Thermostat_RGB" is licensed under CC BY-NC-SA by [Esteban Welschen].

// Define to which pin of the Arduino the output of the LM35 is connected:
#define sensorPin A0

int red_light_pin= 11;
int green_light_pin = 10;
int blue_light_pin = 9;
int temperature_set = 22; //Temperature reference for the thermostat

void setup() {
  // Begin serial communication at a baud rate of 9600:
  Serial.begin(9600);
  pinMode(red_light_pin, OUTPUT);
  pinMode(green_light_pin, OUTPUT);
  pinMode(blue_light_pin, OUTPUT);
 
}

void loop() {
  // Get a reading from the temperature sensor:
  int reading = analogRead(sensorPin);

  // Convert the reading into voltage:
  float voltage = reading * (5000 / 1024.0);

  // Convert the voltage into the temperature in degree Celsius:
  float temperature = voltage / 10;
  // Light the led in blue color range if temperature under 21,5°C
  if (temperature < temperature_set-0.5) {
    digitalWrite (blue_light_pin, HIGH);
    digitalWrite (green_light_pin, LOW);
    digitalWrite (red_light_pin, LOW);
  }
  // Light the led in red color range if temperature above 22,5°C
  if (temperature > temperature_set+0.5) {
    digitalWrite (red_light_pin, HIGH);
    digitalWrite (green_light_pin, LOW);
    digitalWrite (blue_light_pin, LOW);
  }
  // Light the led in green color range if temperature between 21,5°C & 22,5°C
  if ((temperature <= temperature_set+0.5) && (temperature >=temperature_set-0.5)){
    digitalWrite (green_light_pin, HIGH);
    digitalWrite (blue_light_pin, LOW);
    digitalWrite (red_light_pin,LOW);
  }
  // Print the temperature in the Serial Monitor:
  Serial.print(temperature);
  Serial.print(" \xC2\xB0"); // shows degree symbol
  Serial.println("C");

  delay(1000); // wait a second between readings
  
}
