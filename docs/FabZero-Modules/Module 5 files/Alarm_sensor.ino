/* FILE: Alarm sensor
 * 
 * AUTHOR: Esteban Welschen <esteban.welschen@ulb.be>
 * 
 * DATE: 2022-03-22
 * 
 * LICENSE : Creative Commons Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
 * -----------
 *
 * Detects if the sensor has been tilted or not and
 * lights up the LED and the Buzz if so. If someone incline the tilt sensor the buzz rings,
 * the buzz stops if the tilt is returned to its original position.
 * The LED still lights up even if the tilt is returned to its position.
 * To switch off the light the reset button needs to be pushed.
 *
 */

int ledPin = 13;
int buzzPin = 8;
int inPin = 7; 
int value = 0;

void setup() 
{
  pinMode(ledPin, OUTPUT);              // initializes digital pin 13 as output
  pinMode(buzzPin, OUTPUT);             // initializes digital pin 8 as output
  pinMode(inPin, INPUT);                // initializes digital pin 7 as input
}
void loop() 
{
  value = digitalRead(inPin);
  if (digitalRead(inPin) == 1) {
    digitalWrite(ledPin, value);
    digitalWrite(buzzPin, value);
  }
  else {
    digitalWrite(buzzPin, value);
  }
}
