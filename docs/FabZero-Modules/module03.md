# 3. Découpe assistée par ordinateur
## 3.1 Group assignment - Epilog Fusion Pro 32

Our first assignment this week was to choose a material and determine the best parameters for our specific machine (speed, power, kerf, ...)

We will work with svg files (Scalable Vector Graphics). The program we will use is InkScape, but we could also use OpenSCAD.

### 3.1.1 Create the test bench

The first step is to sketch the piece we want to cut. We decide to do a grid of 5x5 squares on InkScape.

To be able to use it with the laser cutter, we need to use a vectorial image. So we export it this way:

![ ](../images/module3/group/vectorial.jpg)

Next, we need to color each square outline. Each one will have a different color because we want to be able to test each combination of force and speed at once.

! [ ](../images/module3/group/colorsquare.jpg)

We chose to use a gray gradient for our different colors.

We export our sketch into SVG. Using a USB key we open our file on the Epilog's computer.

### 3.1.2 Cut the test bench (first try)

We open the file and we go to *FILE>PRINT* and we select the machine.

![ ](../images/module3/group/fileprint.jpg)

Next, we get into the Epilog's Software. We need now to set the parameters.

First, pushing on the sketch, we have the possibility to split this sketch by colors. Then, we can determine the parameters for each subpart (each part of the sketch having a different color).

For each one, we choose those parameters :

- **process type** : *vector*
- **speed** : we go from 10 to 90 (step of 20)
- **power** : we go from 10 to 90 (step of 20)

Next, we choose to go with orange cardboard. We put it into the laser cutter (careful to put it flat in the machine to keep a constant distance between the cardboard and the laser).

On the software, we check with the video how to place the sketch. After, we check that the borders are well placed (those borders defines the region the laser can go into).

We choose for the parameter **Auto Focus** : *plunger*. Then, we *print* to add the file to the Epilog machine.

Comme nous pouvons le voir sur la photo suivante, nous avons allumé l'extracteur de fumée, nous avons activé la machine grâce à la clé, nous avons bien fermé la machine en appuyant dessus et en vérifiant que les LED correspondantes étaient bien allumées.

![ ](../images/module3/group/careful.jpg)


We choose the file on the Epilog machine and we start.

![ ](../images/module3/group/start.jpg)

After having cut the piece, we can delete the file from the machine.

![ ](../images/module3/group/test1.jpg)

The result is a bit disappointing, almost every square is simply totally cut through. It is simply because the cardboard is too thin and the laser set on a too high power

### 3.1.3 Cut the test bench (second try)

For our second try, we decided to focus ourself on a small part of the power range.

![ ](../images/module3/group/drawing.jpg)

This time, the parameters are :

- **process type** : *vector*
- **speed** : we go from 30 to 90 (step of 15)
- **power** : we go from 5 to 25 (step of 5)

![ ](../images/module3/group/test2.jpg)

The result is much more conclusive. Some of the square we very loose and fell down as soon as we picked up the test bench, but we have much more samples to study. We can see that if we go too slow, it will inevitably cut through the paper.

### cut the test bench (third try)

For the second part of the assignment, the Kirigamis, we still lack one important information, what is are the best parameters to be able to easily fold our cardboard without simply cutting it.

We designed a simple rectangle with 5 lines, each having different parameters. We chose those parameters based on our previous test bench, we selected the squares that were not cut through but still quite deeply cut.

![ ](../images/module3/group/PLI.jpg)

The parameters are :
* **Process type** : *vector*
* **Speed** : 30, 60, 75, 90, 90
* **Power** : 5, 10, 10, 10, 15

Every cut hangs on correctly, but the best parameters pair seems to be (90,10), as it seems to be a bit more resilient.

## 3.2 Travail individuel

Pour ce module j'ai décidé de travailler sur un polyèdre plus précisément un dodécaèdre pour en faire un dé à 12 faces. Le choix d'un polyèdre convexe n'est pas pris au hasard, lors du test en groupe nous avons testé la pliure du carton que dans un sens, donc dans l'incertitude de pouvoir plier le carton dans l'autre sens, le choix s'est posé d'opter pour ce modèle.

### 3.2.1 Conception 2D

Pour la conception 2D de mon module j'ai décidé d'utilisé un programme que je connaissais déjà, le plugin pour [Rhinocéros](https://www.rhino3d.com/), [Grasshopper](https://www.grasshopper3d.com/).

Ce plugin permet de créer du dessin paramétrique 2D ou 3D en utilisant de la programmation visuel.

Pour réaliser mon dodécaèdre, je suis parti sur son développement suivant :

![](../images/module3/indi/dode1.jpg)

### 3.2.2 Design grasshopper

Pour le design sur grasshopper je ne vais pas détailler toutes les commandes utilisées, grasshopper est complexe dans son utilisation, pour celui qui serait interessé par son utilisation des [vidéos tutoriels](https://vimeopro.com/rhino/grasshopper-getting-started-by-david-rutten) pour débutant son disponible sur le site de Grasshopper. La liste des commandes est assez variés et importantes vous pouvez trouver toutes les commandes [ici](http://grasshopperdocs.com/).

La première étape sur Grasshopper est de réaliser un polygone à 5 côtés, un pentagone (polygone à 5 côté car on travail en paramétrique et dons le nombre de côté n'est qu'un paramètre comme on le verra plus tard).

![ ](../images/module3/indi/gr1.jpg)

La seconde étape consiste par a créer une copie du premier module, et par une série de transformation basique (rotation, translation) à créer le deuxième pentagone.

**Note :** Sur grasshopper pour translater un élément il faut le faire de manière vectoriel, ils faut donc définir des vecteurs au préalable.

![ ](../images/module3/indi/gr2.jpg)

Les troisième, quatrième, cinquième et sixième étape reposent sur les mêmes principes comme l'on peut le voir sur les images suivantes. Ce sont tous des transformations basiques.

![ ](../images/module3/indi/gr3.jpg)
![ ](../images/module3/indi/gr4.jpg)
![ ](../images/module3/indi/gr5.jpg)
![ ](../images/module3/indi/gr6.jpg)

Précédemment nous avions dit que grasshopper reposait sur du design **paramétrique**. On peut le voir sur les images suivantes, on peut changer le nombre de côté du pentagone pour en faire un triangle, on passe ainsi d'un dodécaèdre à un octaèdre.

![ ](../images/module3/indi/gr7.jpg)
![ ](../images/module3/indi/gr8.jpg)

Il est également facile de changer les dimensions des arrêtes grâce à un simple curseur comme le montre la vidéo suivante.

![](docs/images/module3/indi/dod.mp4)

<video controls>
 <source src="../../images/module3/indi/dod.mp4" type="video/mp4">
</video>

Afin de finaliser le modèle, il est important d'enlever les arrêtes qui se superposent afin d'éviter que le laser passe plusieurs fois sur une même ligne.

![ ](../images/module3/indi/gr9.jpg)

### 3.2.3 Inkscape et découpe du modèle

Avant de lancer la découpe, il faut exporter le fichier en *svg* depuis rhinocéros. Le fichier une fois ouvert sur [Inkscape](https://inkscape.org/fr/) doit encore subir quelques ajustements, différencier les couleurs des arrêtes pour ce qui doit être coupé et ce qui doit être plié, mais cette étape nécessite de découper les chemins car ceux-ci ce sont groupés suite à l'exportation. Les étapes peuvent être visualisé dans la vidéo ci-dessous.
Il existe une série de [tutoriels](https://inkscape.org/learn/tutorials/) pour l'utilisation d'inkskape sur leur site. Mais vu que le design ici a été réalisé sur grasshopper je ne m'attarderai pas sur l'utilisation du logiciel mais uniquement sur le processus pour rendre mon document svg découpable depuis une découpeuse laser.

![](docs/images/module3/indi/ink.mp4)

<video controls>
 <source src="../../images/module3/indi/ink.mp4" type="video/mp4">
</video>

Il faudra encore ajouter les chiffres romains à graver sur les différentes faces dans une troisième couleur.

Pour les paramètres de la découpeuse laser, je me suis basé sur les paramètres choisis lors du test de groupe.

**Pour les découpes :**

* **Process type** : *vector*
* **Speed** : 90
* **Power** : 50

**Pour les plis :**

* **Process type** : *vector*
* **Speed** : 90
* **Power** : 10

**Pour la gravure des chiffres romains :**

**Premier essai**

* **Process type** : *Engrave*
* **Speed** : 90
* **Power** : 5

**Deuxième essai**

* **Process type** : *Vector*
* **Speed** : 90
* **Power** : 5

Deux essais ont été effectués, le premier avec  gravure  de type *"engrave"* des chiffres romains et sans les triangles de collage, le second avec gravure type *"vector"* des chiffres romains et avec les triangles de collage ajouter sur inkskape.

![ ](../images/module3/indi/try1.jpg)

![ ](../images/module3/indi/try2.jpg)

Une fois collés nous obtenons nos deux dés.

![ ](../images/module3/indi/try3.jpg)

![ ](../images/module3/indi/try4.jpg)
</p>
</div>
