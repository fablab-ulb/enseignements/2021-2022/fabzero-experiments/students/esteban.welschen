# Projet final

## 1. Idées de recherche

En me posant la question «Quels sont les problèmes auxquels j’ai pu faire face et qui pourrait rentrer dans  les « [17 Sustainable Development Goals](https://www.un.org/sustainabledevelopment/sustainable-development-goals/) » Nations Unies ? », j’ai pensé à deux problématiques; une plus récente que j’ai rencontré lors de mes études, et une plus ancienne à laquelle j’ai dû faire face durant ma jeunesse, mais qui est toujours d’actualité.

La problématique plus récente est liée aux soirées festives où il est plus que fréquent que des personnes mal intentionnées mettent de la drogue dans des verres. Je connais trop de personnes ayant été impactées pour ne pas m’en soucier.

La seconde problématique, très différente, concerne le nombre de déchets que l’on retrouve dans la nature, dans nos campagnes. Des agriculteurs dans mes connaissances ont déjà perdu des bovins car les canettes métalliques avalées par les bovidés créent des hémorragies internes.

### 1.1 Protection pour verre type Ecocup

Il me semble important que tout le monde puisse se sentir en sécurité en soirée. A ma connaissance, il n'existe actuellement qu’un seul type de protection pour verre sur le commerce. Ce sont des genres de tétines en caoutchouc adaptable sur la plupart des verres ([drink watch](https://drink-watch.com/) par exemple). Toutefois, pour avoir testé ce genre de dispositif, j’ai pu me rendre compte  qu’ils ne sont pas optimaux. Ils posent des problèmes pour le service, la façon de boire est aussi fort différente et ne procure pas les mêmes sensations et finalement les dispositifs élastiques ont parfois tendance à se rompre.

 J’aimerai me pencher sur la question et pouvoir proposer une alternative peut-être pas meilleure mais qui pourrait assurer suffisamment de protection à l’utilisateur. J’ai notamment pensé à un système qui imiterait le principe des bières bavaroises, un simple couvercle qu’on pourrait adapter sur un gobelet de type [Ecocup](https://ecocup.com/fr/produits/modeles-de-gobelets/?gclid=CjwKCAjwlcaRBhBYEiwAK341jY1Pou8Hc0ZoGw9QCwaLepD-qAmiGlMzLNe7gcuX0tUw1vSCQCIpWhoCORAQAvD_BwE).

## 2. Protection pour verre type Ecocup

### 2.1. Problématique : Les verres drogués en soirée

Il arrive de plus en plus fréquemment que des verres soit drogués en soirée et notamment dans les soirées étudiantes. La plupart des étudiants utilisent aujourd'hui des verres réutilisables de la marque [Ecocup](https://ecocup.com/fr/produits/modeles-de-gobelets/?gclid=CjwKCAjwlcaRBhBYEiwAK341jY1Pou8Hc0ZoGw9QCwaLepD-qAmiGlMzLNe7gcuX0tUw1vSCQCIpWhoCORAQAvD_BwE) au format 25 cL. Les questions qu'on pourrait se poser sont les suivantes :

- Existe-t-il des moyens de préventions actuellement, et quels sont-ils ? Si oui, sont-ils utilisés fréquemment ou pas pourquoi ?
- Quels sont les drogues présentes lors de ces soirées ?
- Quels sont les techniques employées par les drogueurs ?
- Comment se prémunir ?

Pour moi, les deux questions les plus pertinentes sont la première et la dernière. Il me parait plus difficile et moins intéressant de répondre aux deux autres. Je souhaite plus basée ma recherche sur la prévention.

### 2.1.1 Les moyens de préventions actuels

Il existe des moyens de protection comme :

- Les [drink watch](https://drink-watch.com/) à 7,90€ pour 3
- [My Safe Cup](https://mysafecup.com/) à 9,90€

Il existe aussi des moyens de détections (qui ne détectent souvent pas l'ensemble des drogues) comme :

- [SipChip](https://www.undercovercolors.com/pages/how-it-works) à 14,99$ pour 3 (non réutilisable)
- [Xantus](https://www.xantus-drinkcheck.de/en/collections/all) à 2,49 € (non réutilisable)

### 2.1.2 Comment se prémunir contre les drogues mises à son insu dans les verres ?

Il existe plusieurs moyens de préventions des risques, on peut soit agir contre le drogueur, soit défendre la potentielle victime. Dans mon cas il me semble plus simple d'agir sur la défense de la potentielle victime. Je vais donc me diriger vers cette option.

#### 2.1.2.1 Les moyens de protection

On peut utiliser des moyens de protection pour verre, ce qui empêche le drogueur de mettre quelque chose dans son verre.

Les questions avec des dispositifs pour verres sont les suivantes :

- Adaptabilités à différents formats de verres
- Facilité d'utilisation (est-ce qu'il est facile de boire, est-ce que le service est facile, ...)
- Solidité du dispositif et sa réutilisation (durée de vie du dispositif)
- Hygiène et nettoyage (est-ce que le dispositif peut s'enlever et être nettoyé facilement)
- Coût (le coût du dispositif doit rester le plus bas possible)

#### 2.1.2.2 Les moyens de détections

Certains moyens de détections peuvent être utilisés avant que la drogue ne soit ingérée. On peut utiliser certains réactifs qui vont agir avec les drogues, et prévenir par un signal visuel l'utilisateur que son verre a été drogué. Ces réactifs sont des procédés chimiques. Il faut donc impérativement connaître les drogues utilisés pour pouvoir utiliser les bons réactifs.

D'autres questions peuvent se poser, comme celle de l'efficacité (est-ce que le réactif est sûr à 100, 80, 70 %), est-ce que son utilisation est simple en soirée, ou encore est-ce que le dispositif peut être réutilisé ou n'est utilisable qu'une seule soirée.

### 2.2 Conception et prototypage

Pour mon prototypage, j'ai décidé d'utiliser la découpeuse laser car elle est la méthode la plus rapide de fabrication au Fablab. J'ai utilisé le logiciel AutoCAD pour la conception numérique car, ayant l'habitude de l'utiliser, il m'est plus rapide de prototyper.

Les prototypes sont en bois car c'est le matériel que j'avais tout simplement chez moi. Le modèle final sera dans un matériaux foodsafe.

#### 2.2.1 Prototype Χλόη I

Premier prototype basé sur le principe de chope bavaroise.


Le prototype comporte un socle, un anneau de maintien, un couvercle ainsi qu'une anse reliant les différentes parties.

![](images/final/protoa1.jpg)

De légers ajustements ont été nécessaires. J'ai redécoupé à l'aide d'une petite scie l'encoche du couvercle et de l'anneau de maintien pour permettre le passage de la anse.

![](images/final/protoa2.jpg)
![](images/final/protoa3.jpg)
![](images/final/protoa4.jpg)

Après avoir testé le premier prototype, j'ai constaté que plusieurs choses sont à revoir, la anse est trop petite, l'écart entre la anse et le gobelet est trop important, enfin la bière stagne un peu dans le socle.

Malgré ça lors de la soirée où j'ai pu le tester, mon prototype a eu un certain succès. Des personnes m'ont déjà demandé s'il était possible de l'acheter.

On m'a mentionné plusieurs avantages :

- Le couvercle qui permet d'éviter les drogues (cela montre que le but premier du prototype est souligné)
- Le fait que le prototype a un certain charme et ne fait pas seulement dispositif anti-drogue.
- La facilité d'utilisation

On m'a fait également part du fait que le bois ramenait un réel plus au prototype. Il faudra donc voir si des bois foodsafe sont envisageables ou un vernis spécial

#### 2.2.2 Prototypes Χλόη I.I et I.II

Suite à mon premier prototype, j'ai conçu deux autres prototypes en prenant compte les défauts du premier.

Pour le prototype I.I, j'ai rapproché la anse contre les anneaux de maintiens. J'ai également agrandi la anse et placé un petit levier par-dessus le couvercle. Ce levier permet une meilleure maniabilité. Il permet aussi de ne pas toucher la anse puisque le levier n’est pas dans le même plan que le couvercle.

Le socle a également été percé afin d'éviter la stagnation de liquide.

![](images/final/protob1.jpg)
![](images/final/protob2.jpg)

En ce qui concerne le prototype I.II, il est presque identique, mais j'ai rapproché encore plus la anse des anneaux de maintiens. Pour cela j'ai créé un emboîtement entre la anse et les anneaux.

![](images/final/protoc1.jpg)
![](images/final/protoc2.jpg)

Conclusion de l'essai : la anse des deux prototypes est désormais trop grande. Le prototype I.II a l'avantage d'être plus réduit que le prototype I.I. Je dois voir s'il y a moyen d'augmenter la longueur du levier qui est légèrement trop petit.

#### 2.2.3 Prototypes Χλόη I.III

Le prototype Χλόη I.III est une version amélioré des deux précédents modèles I.I et I.II. Il a l'avantage d'avoir une anse qui tient parfaitement dans toutes les mains (plus petite que les anses des deux précédents modèles).

Une anse plus petite permet d'atteindre plus facilement le levier du couvercle. Le levier du couvercle est lui également légèrement plus long.

![](images/final/protod1.jpg)
![](images/final/protod2.jpg)

Les remarques et conclusions de ce modèle sont que le modèle a une bonne tenue en main mais qu'il ne s'adapte que peu à d'autres positions de la main. On peut voir sur les deux photos suivantes que la première tenue est agréable mais pas la seconde.

![](images/final/protod3.jpg)
![](images/final/protod4.jpg)

#### 2.2.4 Prototypes Χλόη II.I et II.II

Les deux prototypes suivants proposent une alternative de anse ouverte, qui permet normalement une tenue agréable dans différentes positions de la main. Un changement au niveau de l'emboîtement de l'anneau du milieu a également été effectué. Désormais, l'emboîtement se retrouve inversé par rapport aux précédents modèles, l'anneau s'enchâsse par l'arrière de la anse ce qui évite un décollement possible quand le gobelet y est placé.

![](images/final/protoe1.jpg)
![](images/final/protoe2.jpg)
![](images/final/protoe3.jpg)

La différence entre les modèles II.I et II.II se situe au niveau du levier. Le protoytpe II.II a un levier désaxé pour atteindre le levier plus facilement avec son pouce si tenue dans la main droite.

![](images/final/protof1.jpg)

 Les différentes positions de la main sont désormais plus aisées par rapport aux anciens modèles. La anse manque cependant d'un peu de longueur pour une bonne tenue. Le modèle II.II n'apporte rien par rapport au modèle II.I. Il a même tendance à déséquilibrer le mouvement du couvercle.

#### 2.2.5 Prototypes Χλόη II.III

 Ce modèle est quasi identique au modèle II.I, mise à part que la anse a été rallongée d'environ 1 cm.

 ![](images/final/protog1.jpg)

 Le modèle a été tester lors du Doudou de Mons (donc en conditions réelles). Il a été un peu mis à rude épreuve, et il n'en ai pas sorti indemne. L'humidité de la bière ainsi que les bousculades de la foule ont fait se décoller le socle du prototype. De plus l'un des deux clous permettant la rotation du couvercle s'est descellé, ne laissant qu'un clou en place. Mais même sans socle le modèle reste utilisable car l'anneau central seul permet le maintien du gobelet. Le modèle a donc pu garder sa fonction jusqu'à la fin.

  ![](images/final/protog2.jpg)

#### 2.2.6 Prototypes Χλόη II.IV

 Le modèle II.IV est une petite amélioration du modèle II.III. La anse a encore été rallongée d'un petit centimètre. Les clous permettant la rotation ont été remplacés par un morceau d'épingle directement enchâssé dans une rainure entre les deux morceaux collés. Les emboîtements ont eux été réalisés plus précisément en prenant en compte l'épaisseur de la découpe laser (grâce à des réglettes témoins comme on peut le voir plus bas).

  ![](images/final/protoh1.jpg)

  ![](images/final/reg1.jpg)
  ![](images/final/reg2.jpg)

 Ce modèle a également été réalisé en plastique (on verra l'assemblage et le type de plastique dans le chapitre sur les matériaux)

  ![](images/final/protoi1.jpg)

 Ce modèle n'a pas encore subi de modifications quant à la solidité du socle car il a été conçu avant le test du précédent modèle.

#### 2.2.7 Prototypes Χλόη II.V ou modèle final

 Il s'agit ici du modèle final. Il existe à la fois en bois et en plastique.

 Ce modèle bénéficie d'un renfort au niveau de son socle. Le modèle est également en partie démontable comme on peut le voir dans la vidéo suivante.

![](images/final/protoj1.jpg)
![](images/final/protoj2.jpg)
![](images/final/protok1.jpg)
![](images/final/protol1.jpg)

![](images/final/montage.mp4)

<video controls>
 <source src="../images/final/montage.mp4" type="video/mp4">
</video>


### 2.3 Matériaux et coût

Le choix du type de matériau va se définir sous plusieurs aspects, la mise-en œuvre ou le montage, le coût, l'esthétique et les propriétés du matériau

### 2.3.1 Bois et multiplex

Le bois et le multiplex apportent chaleur et rusticité au prototype, mais ont le désavantage de devoir être traité pour leur durabilité.

#### 2.3.1.1 Panneau Multiplex

Le panneau Multiplex 4 mm est le matériau que j'ai utilisé durant tout le prototypage du au fait de son faible coût. Il se découpe facilement à la découpeuse laser (sur Epilog: Vitesse 10% ; Puissance 85%) et se grave facilement également (sur Epilog: Vitesse 85% ; Puissance 20%).

Le multiplex doit par en revanche être verni avec un verni foodsafe.

Dans le tableau suivant on peut calculer le coût d'un prototype dans ce matériau.

| Matériau | Prix unitaire | Quantité | Prix final
| :--- | :--- | :--- | :---
| Multiplex 4mm 125cmx61cm (chez Brico) | 9,49 € par plaque | 1/11 plaque | 0,8627 €
| Colle à bois imperméable Pattex blanc 250g (chez Brico)| 9,29€ | 0,0085 m<sup>2</sup> (150 g/m<sup>2</sup>) | 0,0474 €
| Boîte de 500 épingles (chez colruyt) | 2 € | 1 épingle | 0,004 €
| Rayher Vernis Transparent Food-Safe 50 mL (chez AVA) | 9,99 € | 0,0285 m<sup>2</sup> (12 m<sup>2</sup>/L) | 0,4745 €
| Prototype en multiplex | | | 1,39 €

#### 2.3.1.2 Panneau Bambou

Le panneau en bambou offre un matériau naturellement foodsafe. Il est d'un aspect moins rustique et beaucoup plus moderne. Le panneau de bambou résiste assez bien à l'humidité sans traitement.

Dans le tableau suivant on peut calculer le coût d'un prototype dans ce matériau.

| Matériau | Prix unitaire | Quantité | Prix final
| :--- | :--- | :--- | :---
| Panneau Bambou 2,5 mm 122cmx244cm (chez Gede-bois | 189 € par plaque | 1/48 plaque | 3,9375 €
| Colle à bois imperméable Pattex blanc 250g (chez Brico)| 9,29€ | 0,0085 m<sup>2</sup> (150 g/m<sup>2</sup>) | 0,0474 €
| Boîte de 500 épingles (chez colruyt) | 2 € | 1 épingle | 0,004 €
| Prototype en bambou | | | 3,99 €

### 2.3.2 Plastiques

Les matériaux plastiques ont l'avantage de mieux résister à l'humidité et donc au lavage. Cependant le plastique à certains inconvénients. Il faut par exemple savoir quelles méthodes d'assemblage utiliser suivant le type de plastique.

### 2.3.2.1 PMMA ou verre acrylique

Le [PMMA](https://fr.wikipedia.org/wiki/Polym%C3%A9thacrylate_de_m%C3%A9thyle) à l'avantage de très bien se couper à la découpeuse laser (sur Epilog: Vitesse 10% ; Puissance 90%). C'est un matériau foodsafe, mais sa non résistance à l'alcool peut constituer un problème pour notre porte-gobelet. On peut voir sur les images suivantes qu'au contact d'un alcool concentré le pmma se fissure. Le test a été réalisé avec de la bière ou la concentration est moindre et le pmma à résister. Le pmma a aussi comme désavantage de se casser assez facilement comme j'ai pu le constater en sortant mes pièces de la découpeuse laser.

![](images/final/plexi1.jpg)
![](images/final/plexi2.jpg)

Dans le tableau suivant on peut calculer le coût du prototype en PMMA.

| Matériau | Prix unitaire | Quantité | Prix final
| :--- | :--- | :--- | :---
| PMMA PL CRISTAL 3 mm 50cmx50cm (chez Brico) | 14,99 € par plaque | 1/4 plaque | 3,75 €
| Colle de plexiglas Acrifix 192 (100g)(chez plexiglas-shop)| 9,50 € | 0,0085 m<sup>2</sup> (150 g/m<sup>2</sup> pas de données on surestime en mettant la même chose que la colle à bois) | 0,12 €
| Boîte de 500 épingles (chez colruyt) | 2 € | 1 épingle | 0,004 €
| Prototype en PMMA | | |  3,874€

### 2.3.2.1 PP ou polypropylène et polyéthylène haute densite PEHD

Le [Polypropylène](https://fr.wikipedia.org/wiki/Polypropyl%C3%A8ne) est un matériau foodsafe utilisé dans de nombreux emballages alimentaires (les Ecocups sont notamment fait en polypropylène). Le polypropylène se coupe un peu plus difficilement à la découpeuse laser (sur Epilog: Vitesse 5% ; Puissance 90%). Lors de la découpe du prototype, j'ai utilisé une plaque de récupération que je pensais être du Polyéthylène haute densité (les deux peuvent avoir le même aspect et sentent tous deux la paraffine lorsqu'ils sont chauffés). J'ai remarqué que ce n'était pas du polyéthylène haute densité lorsque j'ai tenté de souder thermiquement mon prototype (cela ne réagissait pas comme du PEHD j'en ai donc conclu que c'était probablement du polypropylène).

Deux possibilités sont possibles pour assembler les plaques de PP : les coller avec une colle spéciale (cyanoacrylate) et son réactif ou les souder par perçage thermique (avec un vieux fer à souder ou avec une pique chauffer à haute température).

J'ai testé les deux techniques. La première est propre mais il faut voir sur la durée si la colle se maintien. La seconde est moins propre et plus fastidieuse (j'ai fait chauffer un clou sur une bougie puis percé les plaques de part en part)

![](images/final/pp1.jpg)
![](images/final/pp2.jpg)

Malheureusement, je ne sais pas calculer le prix du modèle en PP car je n'ai pas le prix des plaques.

Le [Polyéthylène haute densité](https://fr.wikipedia.org/wiki/Poly%C3%A9thyl%C3%A8ne_haute_densit%C3%A9?tableofcontents=0) estégalement  un matériau foodsafe, et est aussi présent dans de nombreux emballages alimentaires. Il est disponible en de nombreux coloris et est recyclable comme on peut le voir sur le site de [plastic factory](https://www.plasticfactory.be/gallery).

Le PEHD peut être collé et soudé de la même manière que le PP.  J'aurai aimé tester une autre méthode de soudure sur le PEHD, mais je n'ai pas pu la mettre en place sur un des prototypes. La technique consistait à faire chauffer les faces jusqu'à qu'elles deviennent collantes et les coller ensemble. J'ai testé avec de simples bouchons en PEHD sur une taque électrique, cela a très bien fonctionné les bouchons se sont soudés proprement ensemble.

![](images/final/pehd1.jpg)

Dans le tableau suivant, on peut calculer le coût du prototype en PEHD.

| Matériau | Prix unitaire | Quantité | Prix final
| :--- | :--- | :--- | :---
| PEHD noir 3mm 50cmx50cm (plaqueplastique.be) | 8,57 € par plaque | 1/4 plaque | 2,14 €
| Colle plastique Pattex Uni-rapide 2gr (chez brico)| 8,79 € | Possiblité de coller 3-4 prototypes | 2,20 €
| Boîte de 500 épingles (chez colruyt) | 2 € | 1 épingle | 0,004 €
| Prototype en PEHD par soudure | | |  2,144 €
| Prototype en PEHD avec colle | | |  4,344 €

Le PEHD peut également être moulé à chaud. Une alternative serait ainsi envisageable. Un test réalisé en faisant fondre du PEHD et en le moulant dans des chutes de découpes donne déjà un aperçu du potentiel.

![](images/final/moul1.jpg)
![](images/final/moul2.jpg)

</p>
</div>
